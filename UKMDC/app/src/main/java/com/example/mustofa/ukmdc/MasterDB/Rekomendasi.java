package com.example.mustofa.ukmdc.MasterDB;

/**
 * Created by Ardha on 4/10/2016.
 */
public class Rekomendasi
{
    private int ID_rekomendasi, ID_user;

    public Rekomendasi (int ID_rekomendasi, int ID_user)
    {
        setID_rekomendasi(ID_rekomendasi);
        setID_user(ID_user);
    }

    public int getID_rekomendasi() {
        return ID_rekomendasi;
    }

    public void setID_rekomendasi(int ID_rekomendasi) {
        this.ID_rekomendasi = ID_rekomendasi;
    }

    public int getID_user() {
        return ID_user;
    }

    public void setID_user(int ID_user) {
        this.ID_user = ID_user;
    }
}
