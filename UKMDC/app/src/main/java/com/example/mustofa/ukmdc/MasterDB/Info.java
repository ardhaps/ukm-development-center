package com.example.mustofa.ukmdc.MasterDB;

/**
 * Created by Ardha on 4/10/2016.
 */
public class Info
{
    private int ID_info;
    private String info;
    private String time;
    private String date;
    private String foto_sumber;
    private String nama_sumber;

    public Info (int ID_info, String nama_sumber, String info, String time, String date, String foto_sumber)
    {
        setID_info(ID_info);
        setNama_sumber(nama_sumber);
        setInfo(info);
        setTime(time);
        setDate(date);
        setFoto(foto_sumber);
    }

    public int getID_info() {
        return ID_info;
    }

    public void setID_info(int ID_info) {
        this.ID_info = ID_info;
    }

    public String getNama_sumber() {
        return nama_sumber;
    }

    public void setNama_sumber(String nama_sumber) {
        this.nama_sumber = nama_sumber;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date; }

    public String getFoto() {
        return foto_sumber;
    }

    public void setFoto(String foto_sumber) {
        this.foto_sumber = foto_sumber;
    }



}

