package com.example.mustofa.ukmdc.MasterDB;

/**
 * Created by Ardha on 4/10/2016.
 */
public class Mendapatkan
{
    private int ID_info, ID_user;

    public Mendapatkan (int ID_info, int ID_user)
    {
        setID_info(ID_info);
        setID_user(ID_user);
    }

    public int getID_info() {
        return ID_info;
    }

    public void setID_info(int ID_info) {
        this.ID_info = ID_info;
    }

    public int getID_user() {
        return ID_user;
    }

    public void setID_user(int ID_user) {
        this.ID_user = ID_user;
    }
}
