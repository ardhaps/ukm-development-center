package com.example.mustofa.ukmdc.MasterDB;

/**
 * Created by Ardha on 4/10/2016.
 */
public class Posting
{
    private int ID_posting, ID_user, isBranding;
    private String isi, tanggal_posting, foto, nama_lengkap;

    public Posting (int ID_posting, int ID_user, String nama_lengkap, String isi, String tanggal_posting, String foto, int isBranding)
    {
        setID_posting(ID_posting);
        setID_user(ID_user);
        setNama_lengkap(nama_lengkap);
        setIsi(isi);
        setTanggal_posting(tanggal_posting);
        setFoto(foto);
        setIsBranding(isBranding);
    }

    public int getID_posting() {
        return ID_posting;
    }

    public void setID_posting(int ID_posting) {
        this.ID_posting = ID_posting;
    }

    public int getID_user() {
        return ID_user;
    }

    public void setID_user(int ID_user) {
        this.ID_user = ID_user;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }

    public String getTanggal_posting() {
        return tanggal_posting;
    }

    public void setTanggal_posting(String tanggal_posting) {
        this.tanggal_posting = tanggal_posting;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public int getIsBranding() {
        return isBranding;
    }

    public void setIsBranding(int isBranding) {
        isBranding = isBranding;
    }

    public String  getNama_lengkap() { return nama_lengkap; }

    public void setNama_lengkap(String nama_lengkap) {
        this.nama_lengkap = nama_lengkap;
    }
}
