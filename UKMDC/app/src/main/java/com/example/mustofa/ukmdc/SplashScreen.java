package com.example.mustofa.ukmdc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.mustofa.ukmdc.MasterDB.DatabaseHandler;

public class SplashScreen extends AppCompatActivity {
    Intent intent;
    DatabaseHandler databaseHandler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        databaseHandler = new DatabaseHandler(this);
        Thread timerThread = new Thread(){
            public void run(){
                try{
                    sleep(2000);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }finally{
                    if(databaseHandler.getUserCount() == 0)
                        intent = new Intent(SplashScreen.this,Login.class);
                    else
                        intent = new Intent(SplashScreen.this,MainMenu.class);
                    startActivity(intent);
                }

            }
        };
        timerThread.start();
    }
    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }
}
