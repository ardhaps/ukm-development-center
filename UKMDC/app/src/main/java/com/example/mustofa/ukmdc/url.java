package com.example.mustofa.ukmdc;

import android.app.Application;

/**
 * Created by alif.sip on 24/3/2016.
 */
public class url extends Application
{
    private static url singleton;

    public static url getInstance() {
        return singleton;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;
    }
}
