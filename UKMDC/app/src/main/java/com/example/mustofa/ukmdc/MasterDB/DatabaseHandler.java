package com.example.mustofa.ukmdc.MasterDB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ardha on 4/10/2016.
 */
public class DatabaseHandler extends SQLiteOpenHelper
{
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "UKMDC",
    TABLE_USER = "user",
    KEY_ID_USER = "ID_user",
    KEY_NAMA_LENGKAP = "nm_lengkap",
    KEY_EMAIL = "email",
    KEY_PASSWORD = "password",
    KEY_PEKERJAAN = "pekerjaan",
    KEY_LEVEL_USER = "level_user",
    KEY_TGL_LAHIR = "tgl_lahir",
    KEY_MODAL = "modal",
    KEY_FOTO_USER = "foto_user",
    KEY_ISPRODUCT = "isProduct",
    KEY_ISIDEA = "isIdea",
    /******************************/
    /******************************/
    TABLE_UKM = "ukm",
    KEY_ID_UKM = "ID_ukm",
    KEY_NAMA_UKM = "nama_ukm",
    KEY_BIDANG_UKM = "bidang_ukm",
    KEY_NO_TELP = "no_telp",
    KEY_ALAMAT_PROVINSI = "alamat_provinsi",
    KEY_ALAMAT_KOTA = "alamat_kota",
    /******************************/
    /******************************/
    TABLE_POSTING = "posting",
    KEY_ID_POSTING = "ID_posting",
    KEY_ISI = "isi",
    KEY_TANGGAL_POSTING = "tanggal_posting",
    KEY_ISBRANDING = "isBranding",
    KEY_FOTO_POSTING = "foto_posting",
    /******************************/
    /******************************/
    TABLE_INFO = "info",
    KEY_ID_INFO = "ID_info",
    KEY_INFO = "info",
    KEY_TIME = "time",
    KEY_DATE = "date",
    /******************************/
    /******************************/
    TABLE_SUMBER = "sumber",
    KEY_ID_SUMBER = "ID_sumber",
    KEY_NAMA_SUMBER = "nama_sumber",
    KEY_FOTO_SUMBER = "foto",
    /******************************/
    /******************************/
    TABLE_REKOMENDASI = "rekomendasi",
    KEY_ID_REKOMENDASI = "ID_rekomendasi",
    /******************************/
    /******************************/
    TABLE_FOLLOW = "follow",
    KEY_ID_FOLLOW = "ID_follow",
    KEY_ID_USER_FOLLOWED = "ID_user_followed",
    /******************************/
    /******************************/
    TABLE_MENDAPATKAN = "mendapatkan";
    /******************************/
    /******************************/
    //TABLE_MENJALANKAN = "menjalankan";



    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_USER +
        "(" + KEY_ID_USER + " INTEGER PRIMARY KEY NOT NULL," +
                KEY_NAMA_LENGKAP + " STRING NOT NULL," +
                KEY_EMAIL + " STRING NOT NULL," +
                KEY_PASSWORD + " STRING NOT NULL," +
                KEY_PEKERJAAN + " STRING NOT NULL," +
                KEY_LEVEL_USER + " STRING NOT NULL," +
                KEY_TGL_LAHIR + " STRING NOT NULL," +
                KEY_MODAL + " STRING NOT NULL," +
                KEY_FOTO_USER + " TEXT NOT NULL," +
                KEY_ISIDEA + " INTEGER NOT NULL," +
                KEY_ISPRODUCT + " INTEGER NOT NULL)"
        );

        db.execSQL("CREATE TABLE " + TABLE_UKM +
        "(" + KEY_ID_UKM + " INTEGER PRIMARY KEY NOT NULL," +
                KEY_ID_USER + " STRING NOT NULL," +
                KEY_NAMA_UKM + " STRING NOT NULL," +
                KEY_BIDANG_UKM + " STRING NOT NULL," +
                KEY_NO_TELP + " STRING NOT NULL," +
                KEY_ALAMAT_PROVINSI + " STRING NOT NULL," +
                KEY_ALAMAT_KOTA + " STRING NOT NULL)"
        );

        db.execSQL("CREATE TABLE " + TABLE_POSTING +
                "(" + KEY_ID_POSTING + " INTEGER PRIMARY KEY NOT NULL," +
                KEY_ID_USER + " INTEGER NOT NULL," +
                KEY_NAMA_LENGKAP + " STRING NOT NULL," +
                KEY_ISI + " STRING NOT NULL," +
                KEY_TANGGAL_POSTING + " STRING NOT NULL," +
                KEY_ISBRANDING + " INTEGER NOT NULL," +
                KEY_FOTO_POSTING + " TEXT NOT NULL)"
        );

        db.execSQL("CREATE TABLE " + TABLE_INFO +
                "(" + KEY_ID_INFO + " INTEGER PRIMARY KEY NOT NULL," +
                KEY_NAMA_SUMBER + " TEXT NOT NULL," +
                KEY_INFO + " TEXT NOT NULL," +
                KEY_TIME + " STRING NOT NULL," +
                KEY_DATE + " STRING NOT NULL," +
                KEY_FOTO_SUMBER + " STRING NOT NULL)"
        );

        db.execSQL("CREATE TABLE " + TABLE_SUMBER +
                "(" + KEY_ID_SUMBER + " INTEGER PRIMARY KEY NOT NULL," +
                 KEY_NAMA_SUMBER + " STRING NOT NULL," +
                KEY_FOTO_SUMBER + " TEXT NOT NULL)"
        );

        db.execSQL("CREATE TABLE " + TABLE_REKOMENDASI +
                "(" + KEY_ID_REKOMENDASI + " INTEGER PRIMARY KEY NOT NULL," +
                KEY_ID_USER + " INTEGER NOT NULL)"
        );

        db.execSQL("CREATE TABLE " + TABLE_FOLLOW +
                "(" + KEY_ID_FOLLOW + " INTEGER PRIMARY KEY NOT NULL," +
                KEY_ID_USER + " INTEGER NOT NULL," +
                KEY_ID_USER_FOLLOWED + " INTEGER NOT NULL)"
        );

        db.execSQL("CREATE TABLE " + TABLE_MENDAPATKAN +
                        "(" + KEY_ID_USER + " INTEGER PRIMARY KEY NOT NULL," +
                        KEY_ID_INFO + " INTEGER NOT NULL)"
        );

        /*db.execSQL("CREATE TABLE " + TABLE_MENJALANKAN +
                "(" + KEY_ID_USER + " INTEGER PRIMARY KEY NOT NULL," +
                KEY_ID_UKM + " INTEGER PRIMARY KEY NOT NULL)"
        );*/
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }

    public void createUser (User user)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put (KEY_ID_USER, user.getID_user());
        value.put (KEY_NAMA_LENGKAP, user.getNama_lengkap());
        value.put (KEY_EMAIL, user.getEmail());
        value.put (KEY_PASSWORD, user.getPassword());
        value.put (KEY_PEKERJAAN, user.getPekerjaan());
        value.put (KEY_LEVEL_USER, user.getLevel_user());
        value.put (KEY_TGL_LAHIR, user.getTgl_lahir());
        value.put (KEY_MODAL, user.getModal());
        value.put (KEY_FOTO_USER, user.getFoto_user());
        value.put (KEY_ISIDEA, user.getIsIdea());
        value.put (KEY_ISPRODUCT, user.getIsProduct());

        db.insert(TABLE_USER, null, value);
        value.clear();
        db.close();
    }

    public int updateUser (User user)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put (KEY_ID_USER, user.getID_user());
        value.put (KEY_NAMA_LENGKAP, user.getNama_lengkap());
        value.put (KEY_EMAIL, user.getEmail());
        value.put (KEY_PASSWORD, user.getPassword());
        value.put (KEY_PEKERJAAN, user.getPekerjaan());
        value.put (KEY_LEVEL_USER, user.getLevel_user());
        value.put (KEY_TGL_LAHIR, user.getTgl_lahir());
        value.put (KEY_MODAL, user.getModal());
        value.put (KEY_FOTO_USER, user.getFoto_user());
        value.put (KEY_ISIDEA, user.getIsIdea());
        value.put(KEY_ISPRODUCT, user.getIsProduct());

        int isUpdate = db.update(TABLE_USER, value, KEY_ID_USER + "=?", new String[] {String.valueOf(user.getID_user())});
        value.clear();
        db.close();

        return isUpdate;
    }
    public int getUserCount() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_USER, null);
        int count = cursor.getCount();
        cursor.close();
        db.close();
        return count;
    }

    public void deleteUser ()
    {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_USER, null, null);
        db.close();
    }

    public User getUser()
    {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_USER, new String[]{KEY_ID_USER, KEY_NAMA_LENGKAP, KEY_EMAIL, KEY_PASSWORD, KEY_PEKERJAAN, KEY_LEVEL_USER, KEY_TGL_LAHIR, KEY_MODAL, KEY_FOTO_USER, KEY_ISIDEA, KEY_ISPRODUCT},null,null,null,null,null,null);

        if (cursor == null)
            return null;
        cursor.moveToFirst();

        User user = new User(cursor.getInt(0), cursor.getString(1), cursor.getString(2),
                cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6),
                cursor.getString(7), cursor.getString(8), cursor.getInt(9), cursor.getInt(10));

        cursor.close();
        db.close();

        return user;
    }

    public void createUkm(Ukm ukm)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put (KEY_ID_UKM, ukm.getID_ukm());
        value.put (KEY_ID_USER, ukm.getID_user());
        value.put (KEY_NAMA_UKM, ukm.getNama_ukm());
        value.put (KEY_BIDANG_UKM, ukm.getBidang_ukm());
        value.put (KEY_NO_TELP, ukm.getNo_telp());
        value.put (KEY_ALAMAT_PROVINSI, ukm.getAlamat_provinsi());
        value.put (KEY_ALAMAT_KOTA, ukm.getAlamat_kota());

        db.insert(TABLE_UKM, null, value);
        value.clear();
        db.close();
    }

    public int updateUkm (Ukm ukm)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues value = new ContentValues();
        value.put (KEY_ID_UKM, ukm.getID_ukm());
        value.put (KEY_ID_USER, ukm.getID_user());
        value.put (KEY_NAMA_UKM, ukm.getNama_ukm());
        value.put (KEY_BIDANG_UKM, ukm.getBidang_ukm());
        value.put (KEY_NO_TELP, ukm.getNo_telp());
        value.put (KEY_ALAMAT_PROVINSI, ukm.getAlamat_provinsi());
        value.put(KEY_ALAMAT_KOTA, ukm.getAlamat_kota());

        int isUpdate = db.update(TABLE_USER, value, KEY_ID_UKM + "=?",new String[]{String.valueOf(ukm.getID_ukm())});
        value.clear();
        db.close();

        return isUpdate;
    }

    public void deleteUkm ()
    {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_UKM, null, null);
        db.close();
    }

    public Ukm getUkm()
    {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_UKM, new String[]{KEY_ID_UKM, KEY_ID_USER, KEY_NAMA_UKM, KEY_BIDANG_UKM, KEY_NO_TELP, KEY_ALAMAT_PROVINSI, KEY_ALAMAT_KOTA},null,null,null,null,null,null);

        if (cursor == null)
            return null;
        cursor.moveToFirst();

        Ukm ukm = new Ukm(cursor.getInt(0), cursor.getInt(1), cursor.getString(2),
                cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6));

        cursor.close();
        db.close();

        return ukm;
    }

    public void createPosting (Posting posting)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues value = new ContentValues();

        value.put(KEY_ID_POSTING, posting.getID_posting());
        value.put(KEY_ID_USER, posting.getID_user());
        value.put(KEY_NAMA_LENGKAP, posting.getNama_lengkap());
        value.put(KEY_ISI, posting.getIsi());
        value.put(KEY_TANGGAL_POSTING, posting.getTanggal_posting());
        value.put(KEY_FOTO_POSTING, posting.getFoto());
        value.put(KEY_ISBRANDING, posting.getIsBranding());


        long i = db.insert(TABLE_POSTING, null, value);
        value.clear();
        db.close();
    }

    public int updatePosting (Posting posting)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues value = new ContentValues();

        value.put(KEY_ID_POSTING, posting.getID_posting());
        value.put(KEY_ID_USER, posting.getID_user());
        value.put(KEY_NAMA_LENGKAP, posting.getNama_lengkap());
        value.put(KEY_ISI, posting.getIsi());
        value.put(KEY_TANGGAL_POSTING, posting.getTanggal_posting());
        value.put(KEY_FOTO_POSTING, posting.getFoto());
        value.put(KEY_ISBRANDING, posting.getIsBranding());

        int isUpdate = db.update(TABLE_POSTING, value, KEY_ID_POSTING + "=?", new String[]{String.valueOf(posting.getID_posting())});
        value.clear();
        db.close();

        return  isUpdate;
    }

    public void deletePosting ()
    {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_POSTING, null, null);
        db.close();
    }
    public int getPostingCount() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_POSTING, null);
        int count = cursor.getCount();
        cursor.close();
        db.close();
        return count;
    }
    public List<Posting> getAllPosting()
    {
        SQLiteDatabase db = getReadableDatabase();
        List<Posting> postings = new ArrayList<Posting>();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_POSTING, null);
        if(cursor.moveToFirst())
            do {
                Posting posting = new Posting(cursor.getInt(0), cursor.getInt(1), cursor.getString(2),
                        cursor.getString(3), cursor.getString(4), cursor.getString(6), cursor.getInt(5));
                postings.add(posting);
            }while(cursor.moveToNext());
        cursor.close();
        db.close();

        return postings;
    }

    public Posting getPosting()
    {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_POSTING, new String[]{KEY_ID_POSTING, KEY_ID_USER, KEY_NAMA_LENGKAP, KEY_ISI, KEY_TANGGAL_POSTING, KEY_FOTO_POSTING, KEY_ISBRANDING},null,null,null,null,null,null);

        if (cursor == null)
            return null;
        cursor.moveToFirst();

        Posting posting = new Posting(cursor.getInt(0), cursor.getInt(1), cursor.getString(2),
                cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getInt(6));

        cursor.close();
        db.close();

        return posting;
    }


    public void createInfo (Info info)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues value = new ContentValues();

        value.put(KEY_ID_INFO, info.getID_info());
        value.put(KEY_NAMA_SUMBER, info.getNama_sumber());
        value.put(KEY_INFO, info.getInfo());
        value.put(KEY_TIME, info.getTime());
        value.put(KEY_DATE, info.getDate());
        value.put(KEY_FOTO_SUMBER, info.getFoto());

        db.insert(TABLE_INFO, null, value);
        value.clear();
        db.close();
    }
    public int getInfoCount() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_INFO, null);
        int count = cursor.getCount();
        cursor.close();
        db.close();
        return count;
    }
    public int updateInfo (Info info)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues value = new ContentValues();

        value.put(KEY_ID_INFO, info.getID_info());
        value.put(KEY_NAMA_SUMBER, info.getNama_sumber());
        value.put(KEY_INFO, info.getInfo());
        value.put(KEY_TIME, info.getTime());
        value.put(KEY_DATE, info.getDate());
        value.put(KEY_FOTO_SUMBER, info.getFoto());
        int isUpdate = db.update(TABLE_INFO, value, KEY_ID_INFO + "=?",new String[]{String.valueOf(info.getID_info())});
        value.clear();
        db.close();

        return  isUpdate;
    }

    public void deleteInfo ()
    {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_INFO, null, null);
        db.close();
    }

    public Info getInfos()
    {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_INFO, new String[]{KEY_ID_INFO, KEY_NAMA_SUMBER, KEY_INFO, KEY_TIME, KEY_DATE, KEY_FOTO_SUMBER},null,null,null,null,null,null);

        if (cursor == null)
            return null;
        cursor.moveToFirst();

        Info info = new Info(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3),
                cursor.getString(4), cursor.getString(5));

        cursor.close();
        db.close();

        return info;
    }

    public List<Info> getAllInfo()
    {
        SQLiteDatabase db = getReadableDatabase();
        List<Info> infos = new ArrayList<Info>();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_INFO, null);
        if(cursor.moveToFirst())
            do {
                Info info = new Info(cursor.getInt(0), cursor.getString(1), cursor.getString(2),
                        cursor.getString(3), cursor.getString(4), cursor.getString(5));
                infos.add(info);
            }
            while(cursor.moveToNext());
        cursor.close();
        db.close();

        return infos;
    }

    public void createSumber (Sumber sumber)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues value = new ContentValues();

        value.put(KEY_ID_SUMBER, sumber.getID_sumber());
        value.put(KEY_NAMA_SUMBER, sumber.getNama_sumber());
        value.put(KEY_FOTO_SUMBER, sumber.getFoto_sumber());

        db.insert(TABLE_SUMBER, null, value);
        value.clear();
        db.close();
    }

    public int updateSumber (Sumber sumber)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues value = new ContentValues();

        value.put(KEY_ID_SUMBER, sumber.getID_sumber());
        value.put(KEY_NAMA_SUMBER, sumber.getNama_sumber());
        value.put(KEY_FOTO_SUMBER, sumber.getFoto_sumber());

        int isUpdate = db.update(TABLE_SUMBER, value, KEY_ID_SUMBER + "=?",new String[]{String.valueOf(sumber.getID_sumber())});
        value.clear();
        db.close();

        return  isUpdate;
    }

    public void deleteSumber ()
    {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_SUMBER, null, null);
        db.close();
    }

    public Sumber getSumber()
    {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_SUMBER, new String[]{KEY_ID_SUMBER, KEY_NAMA_SUMBER, KEY_FOTO_SUMBER},null,null,null,null,null,null);

        if (cursor == null)
            return null;
        cursor.moveToFirst();

        Sumber sumber = new Sumber(cursor.getInt(0), cursor.getString(1), cursor.getString(2));

        cursor.close();
        db.close();

        return sumber;
    }

    public void createRekomendasi (Rekomendasi rekom)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues value = new ContentValues();

        value.put(KEY_ID_REKOMENDASI, rekom.getID_rekomendasi());
        value.put(KEY_ID_USER, rekom.getID_user());

        db.insert(TABLE_REKOMENDASI, null, value);
        value.clear();
        db.close();

    }

    public int updateRekomendasi (Rekomendasi rekom)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues value = new ContentValues();

        value.put(KEY_ID_REKOMENDASI, rekom.getID_rekomendasi());
        value.put(KEY_ID_USER, rekom.getID_user());

        int isUpdate = db.update(TABLE_REKOMENDASI, value, KEY_ID_REKOMENDASI + "=?",new String[]{String.valueOf(rekom.getID_rekomendasi())});
        value.clear();
        db.close();

        return  isUpdate;
    }

    public void deleteRekomendasi ()
    {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_REKOMENDASI, null, null);
        db.close();
    }

    public int getRekomendasiCount ()
    {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_REKOMENDASI, null);
        int count = cursor.getCount();
        cursor.close();
        db.close();

        return count;
    }

    public Rekomendasi getRekomendasi()
    {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_REKOMENDASI, new String[]{KEY_ID_REKOMENDASI, KEY_ID_USER},null,null,null,null,null,null);

        if (cursor == null)
            return null;
        cursor.moveToFirst();

        Rekomendasi rekom = new Rekomendasi(cursor.getInt(0), cursor.getInt(1));

        cursor.close();
        db.close();

        return rekom;
    }


    public void createMendapatkan (Mendapatkan mendapatkan)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues value = new ContentValues();

        value.put(KEY_ID_USER, mendapatkan.getID_user());
        value.put(KEY_ID_INFO, mendapatkan.getID_info());

        db.insert(TABLE_MENDAPATKAN, null, value);
        value.clear();
        db.close();
    }

    public void deleteMendapatkan ()
    {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_MENDAPATKAN, null, null);
        db.close();
    }

    public Mendapatkan getMendapatkan()
    {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_MENDAPATKAN, new String[]{KEY_ID_USER, KEY_ID_INFO},null,null,null,null,null,null);

        if (cursor == null)
            return null;
        cursor.moveToFirst();

        Mendapatkan mendapatkan = new Mendapatkan(cursor.getInt(0), cursor.getInt(1));

        cursor.close();
        db.close();

        return mendapatkan;
    }

    public void createFollow (Follow follow)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues value = new ContentValues();

        value.put(KEY_ID_FOLLOW, follow.getID_follow());
        value.put(KEY_ID_USER, follow.getID_user());
        value.put(KEY_ID_USER_FOLLOWED, follow.getID_user_followed());

        db.insert(TABLE_FOLLOW, null, value);
        value.clear();
        db.close();
    }

    public int updateFollow (Follow follow)
    {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues value = new ContentValues();

        value.put(KEY_ID_FOLLOW, follow.getID_follow());
        value.put(KEY_ID_USER, follow.getID_user());
        value.put(KEY_ID_USER_FOLLOWED, follow.getID_user_followed());

        int isUpdate = db.update(TABLE_FOLLOW, value, KEY_ID_FOLLOW + "=?",new String[]{String.valueOf(follow.getID_follow())});
        value.clear();
        db.close();

        return isUpdate;
    }


    public void deleteFollow ()
    {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_FOLLOW, null, null);
        db.close();
    }

    public Follow getFollow()
    {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_FOLLOW, new String[]{KEY_ID_FOLLOW, KEY_ID_USER, KEY_ID_USER_FOLLOWED},null,null,null,null,null,null);

        if (cursor == null)
            return null;
        cursor.moveToFirst();

        Follow follow = new Follow(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2));

        cursor.close();
        db.close();

        return follow;
    }
    public void deleteAll(){
        deleteUser();
        deleteInfo();
        deletePosting();
    }
}
