package com.example.mustofa.ukmdc;


import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.mustofa.ukmdc.MasterDB.DatabaseHandler;
import com.example.mustofa.ukmdc.MasterDB.Posting;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TimeLine.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TimeLine#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TimeLine extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    List<Posting> postings;
    DatabaseHandler databaseHandler;
    ArrayAdapter<Posting> adapter;
    FloatingActionButton floatingActionButton;
    ListView listView;
    float x1,x2;
    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TimeLine.
     */
    // TODO: Rename and change types and number of parameters
    public static TimeLine newInstance(String param1, String param2) {
        TimeLine fragment = new TimeLine();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public TimeLine() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_time_line, container, false);
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getActionMasked())
                {
                    case MotionEvent.ACTION_DOWN:
                        x1 = event.getX();
                        break;
                    case MotionEvent.ACTION_UP:
                        x2 = event.getX();
                        if(x2>x1) {

                        }
                        else{
                            ((MainMenu)getActivity()).loadFragment(2);
                        }
                        break;
                }
                return true;
            }
        });
        floatingActionButton = (FloatingActionButton) view.findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),NewPost.class));
            }
        });
        databaseHandler = new DatabaseHandler(getActivity());
        listView = (ListView) view.findViewById(R.id.timeline_list);
        instanceView();
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }
    public void instanceView()
    {
        //List<Schedules> fromDatabase = dbHandler.getAllSchedules();
        postings = databaseHandler.getAllPosting();
        //List<Schedules> fromDatabase = sc.jadwalHariIni;
        //int count = dbHandler.getScheduleCount();
        int count = postings.size();
        if(count != 0)
            populateList();
    }
    public void populateList()
    {
        if(adapter == null)
            adapter = new PostingListAdapter();
        listView.setAdapter(adapter);
    }
    private class PostingListAdapter extends ArrayAdapter<Posting> {
        public PostingListAdapter(){
            //super(Schedule.this,R.layout.listview_item,Jadwals);
            super(getActivity(),R.layout.timeline_item,postings);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null)
                convertView = getActivity().getLayoutInflater().inflate(R.layout.timeline_item, parent, false);
            final Posting currentPosting = postings.get(position);
            ImageView user = (ImageView) convertView.findViewById(R.id.tl_userimage);
            /*if(!currentPosting.getFoto().equals(""))
                user.setImageBitmap(decodeImage(currentPosting.getFoto()));*/
            TextView username = (TextView) convertView.findViewById(R.id.tl_username);
            if(!currentPosting.getNama_lengkap().equals(""))
                username.setText(currentPosting.getNama_lengkap());
            TextView aktivitas = (TextView) convertView.findViewById(R.id.tl_aktivitas);
            if(!currentPosting.getIsi().equals(""))
                aktivitas.setText(currentPosting.getIsi());
            ImageView impost = (ImageView) convertView.findViewById(R.id.tl_imagepost);
            if(currentPosting.getFoto().length()<100)
                impost.setVisibility(View.GONE);
            else
                impost.setImageBitmap(decodeImage(currentPosting.getFoto()));
            ImageView imrekom = (ImageView) convertView.findViewById(R.id.iv_rekom);
            return convertView;
        }
    }
    private Bitmap decodeImage(String enImage)
    {
        byte[] decodedString = Base64.decode(enImage, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }
}