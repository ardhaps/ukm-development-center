package com.example.mustofa.ukmdc;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.mustofa.ukmdc.MasterDB.DatabaseHandler;
import com.example.mustofa.ukmdc.MasterDB.GetUserCallback;
import com.example.mustofa.ukmdc.MasterDB.ServerRequest;
import com.example.mustofa.ukmdc.MasterDB.User;

import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {

    public static String base_url = "http://192.168.1.13:8080/ukm-development-center/public/api/v1/";

    public static final String KEY_EMAIL = "email";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_PASSWORD = "password";
    private String mEmail, mPassword;
    DatabaseHandler databaseHandler;
    private EditText edtEmail, edtPassword;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        databaseHandler = new DatabaseHandler(this);
        edtEmail = (EditText) findViewById(R.id.edtEmail);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                LoginUser();
            }
        });
    }

    public void toRegister(View v)
    {
        Intent intent = new Intent(getApplicationContext(),Register.class);
        startActivity(intent);
    }

    private void LoginUser(){
        ServerRequest serverRequest = new ServerRequest(this);
        serverRequest.Login(edtEmail.getText().toString(), edtPassword.getText().toString(), new GetUserCallback() {
            @Override
            public void Done(User user, String pesan) {
                if (pesan.equals("found")) {
                    databaseHandler.createUser(user);
                    startActivity(new Intent(Login.this,SplashScreen.class));
                } else
                    showError(pesan);
            }
        });
    }
    private void showError(String messsage) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage(messsage);
        alert.setPositiveButton("Got It", null);
        alert.show();
    }
}
