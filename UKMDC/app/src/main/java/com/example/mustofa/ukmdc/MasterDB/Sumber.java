package com.example.mustofa.ukmdc.MasterDB;

/**
 * Created by Ardha on 4/10/2016.
 */
public class Sumber
{
    private int ID_sumber;
    private String nama_sumber, foto_sumber;

    public Sumber (int ID_sumber, String nama_sumber, String foto_sumber)
    {
        setID_sumber(ID_sumber);
        setNama_sumber(nama_sumber);
        setFoto_sumber(foto_sumber);
    }

    public int getID_sumber() {
        return ID_sumber;
    }

    public void setID_sumber(int ID_sumber) {
        this.ID_sumber = ID_sumber;
    }

    public String getNama_sumber() {
        return nama_sumber;
    }

    public void setNama_sumber(String nama_sumber) {
        this.nama_sumber = nama_sumber;
    }

    public String getFoto_sumber () {
        return foto_sumber;
    }

    public void setFoto_sumber (String foto) {
        this.foto_sumber = foto_sumber;
    }
}
