package com.example.mustofa.ukmdc;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;

import com.example.mustofa.ukmdc.MasterDB.DatabaseHandler;
import com.example.mustofa.ukmdc.MasterDB.GetUserCallback;
import com.example.mustofa.ukmdc.MasterDB.ServerRequest;
import com.example.mustofa.ukmdc.MasterDB.Ukm;
import com.example.mustofa.ukmdc.MasterDB.User;

import java.text.DateFormatSymbols;
import java.util.Calendar;

public class Kuisioner extends AppCompatActivity {

    LinearLayout produk, bidang, modal, ide,ukm,profil;
    int isProduk,isIde;
    String[] bidangList = new String[]{"Kuliner","Elektronik","Teknologi Informasi","Pertanian","Peternakan","Otomotif","Pendidikan","Kesehatan"};
    String[] provinsiList = new String[]{"Jawa Tengah","Jawa Timur"};
    String[] kotaListJateng = new String[]{"Yogyakarta","Solo","Klaten"};
    String[] kotaListJatim = new String[]{"Kediri","Sumenep","Surabaya"};
    Spinner bidangSp,kotaSp,provinsiSp;
    EditText namaUkmEt,teleponEt, namaEt,pekerjaanEt,emailEt,tgl_lahirEt,passwordEt;
    DatabaseHandler databaseHandler;
    String nModal,namaUkm = "",telepon = "",provinsi = "",kota = "", nama = "", pekerjaan = "", email = "", tgl_lahir = "", password = "",bidangSel = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kuisioner);
        databaseHandler = new DatabaseHandler(this);
        produk = (LinearLayout) findViewById(R.id.kuisioner_produk);
        bidang = (LinearLayout) findViewById(R.id.kuisioner_bidang);
        modal = (LinearLayout) findViewById(R.id.kuisioner_modal);
        ide = (LinearLayout) findViewById(R.id.kuisioner_ide);
        ukm = (LinearLayout) findViewById(R.id.kuisioner_ukm);
        profil = (LinearLayout) findViewById(R.id.kuisioner_profil);
        bidangSp = (Spinner) findViewById(R.id.bidang_pilihan);
        bidangSp.setAdapter(new ArrayAdapter(this,R.layout.custom_spinner,bidangList));
        kotaSp = (Spinner) findViewById(R.id.kota);
        provinsiSp = (Spinner) findViewById(R.id.provinsi);
        provinsiSp.setAdapter(new ArrayAdapter(this,R.layout.custom_spinner,provinsiList));
        provinsiSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        kotaSp.setAdapter(new ArrayAdapter(Kuisioner.this, R.layout.custom_spinner, kotaListJateng));
                        break;
                    default:
                        kotaSp.setAdapter(new ArrayAdapter(Kuisioner.this, R.layout.custom_spinner, kotaListJatim));
                        break;
                }
                provinsi = provinsiSp.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        namaUkmEt = (EditText) findViewById(R.id.namaUkm);
        teleponEt = (EditText) findViewById(R.id.noTelp);
        namaEt = (EditText) findViewById(R.id.nama);
        pekerjaanEt = (EditText) findViewById(R.id.pekerjaan);
        emailEt = (EditText) findViewById(R.id.email);
        tgl_lahirEt = (EditText) findViewById(R.id.tgl_lahir);
        tgl_lahirEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                Dialog mDialog = new DatePickerDialog(Kuisioner.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        tgl_lahirEt.setText(String.valueOf(dayOfMonth) + " " +
                                new DateFormatSymbols().getMonths()[monthOfYear] + " " + String.valueOf(year));
                    }
                },calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH));
                mDialog.show();
            }
        });
        passwordEt = (EditText) findViewById(R.id.password);
    }
    public void lanjut(View view)
    {
        switch(view.getId()){
            case R.id.bt_produk:
                produk.setVisibility(View.GONE);
                if(isProduk == 0)
                    ide.setVisibility(View.VISIBLE);
                else
                    bidang.setVisibility(View.VISIBLE);
                break;
            case R.id.bt_bidang:
                bidangSel = bidangSp.getSelectedItem().toString();
                bidang.setVisibility(View.GONE);
                modal.setVisibility(View.VISIBLE);
                break;
            case R.id.bt_modal:
                modal.setVisibility(View.GONE);
                if(isProduk == 0)
                    profil.setVisibility(View.VISIBLE);
                else
                    ukm.setVisibility(View.VISIBLE);
                break;
            case R.id.bt_ide:
                ide.setVisibility(View.GONE);
                bidang.setVisibility(View.VISIBLE);
                break;
            case R.id.bt_ukm:
                namaUkm = namaUkmEt.getText().toString();
                telepon = teleponEt.getText().toString();
                kota = kotaSp.getSelectedItem().toString();
                provinsi = provinsiSp.getSelectedItem().toString();
                ukm.setVisibility(View.GONE);
                profil.setVisibility(View.VISIBLE);
                break;
            default:
                nama = namaEt.getText().toString();
                pekerjaan = pekerjaanEt.getText().toString();
                email = emailEt.getText().toString();
                password = passwordEt.getText().toString();
                tgl_lahir = tgl_lahirEt.getText().toString();
                User user = new User(0,nama,email,password,pekerjaan,"",nModal,tgl_lahir,"",isProduk,isIde);
                Ukm ukm = new Ukm(0,0,namaUkm,bidangSel,telepon,provinsi,kota);
                ServerRequest serverRequest = new ServerRequest(this);
                serverRequest.Register(user, ukm, new GetUserCallback() {
                    @Override
                    public void Done(User user, String pesan) {
                        if (pesan.equals("found")) {
                            databaseHandler.createUser(user);
                            startActivity(new Intent(Kuisioner.this,SplashScreen.class));
                        } else
                            showError(pesan);
                    }
                });
                break;
        }
    }
    private void showError(String messsage) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage(messsage);
        alert.setPositiveButton("Got It", null);
        alert.show();
    }
    public void onRadioClick(View view)
    {
        switch (view.getId()){
            case R.id.produk_sudah:
                isProduk = 1;
                break;
            case R.id.produk_belum:
                isProduk = 0;
                break;
            case R.id.ide_sudah:
                isIde = 1;
                break;
            case R.id.modal1:
                nModal = "< Rp 1.000.000";
                break;
            case R.id.modal2:
                nModal = "Rp 1.000.000 - Rp 10.000.000";
                break;
            case R.id.modal3:
                nModal = "> Rp 10.000.000";
                break;
            default:
                isIde = 0;
                break;
        }
    }
}
