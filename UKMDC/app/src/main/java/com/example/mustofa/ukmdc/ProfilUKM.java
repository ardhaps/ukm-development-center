package com.example.mustofa.ukmdc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

public class ProfilUKM extends AppCompatActivity {

    private EditText textView2, textView3, textView4, textView5, textView6;
    private ImageView tombol, save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil_ukm);

        textView2 = (EditText) findViewById(R.id.textView2);
        textView3 = (EditText) findViewById(R.id.textView3);
        textView4 = (EditText) findViewById(R.id.textView4);
        textView5 = (EditText) findViewById(R.id.textView5);
        textView6 = (EditText) findViewById(R.id.textView6);
        tombol = (ImageView) findViewById(R.id.imageView5);
        save = (ImageView) findViewById(R.id.imageView6);

        textView2.setEnabled(false);
        textView3.setEnabled(false);
        textView4.setEnabled(false);
        textView5.setEnabled(false);
        textView6.setEnabled(false);

        save.setEnabled(false);

        tombol.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                textView2.setEnabled(true);
                textView3.setEnabled(true);
                textView4.setEnabled(true);
                textView5.setEnabled(true);
                textView6.setEnabled(true);

                save.setEnabled(true);
                tombol.setEnabled(false);
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                textView2.setEnabled(false);
                textView3.setEnabled(false);
                textView4.setEnabled(false);
                textView5.setEnabled(false);
                textView6.setEnabled(false);

                tombol.setEnabled(false);
            }
        });

    }
}
