package com.example.mustofa.ukmdc;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.mustofa.ukmdc.MasterDB.DatabaseHandler;
import com.example.mustofa.ukmdc.MasterDB.GetInfoCallback;
import com.example.mustofa.ukmdc.MasterDB.Info;
import com.example.mustofa.ukmdc.MasterDB.Posting;
import com.example.mustofa.ukmdc.MasterDB.ServerRequest;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link InfoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link InfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InfoFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    List<Info> infos;
    DatabaseHandler databaseHandler;
    ArrayAdapter<Info> adapter;
    ListView listView;
    float x1,x2;
    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment InfoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InfoFragment newInstance(String param1, String param2) {
        InfoFragment fragment = new InfoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public InfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    private void getInfo(){
        if(databaseHandler.getInfoCount() == 0) {
            ServerRequest serverRequest = new ServerRequest(getActivity());
            serverRequest.getInfo(new GetInfoCallback() {
                @Override
                public void Done(List<Info> infos, String pesan) {
                    if (pesan.equals("success")) {
                        for (int i = 0; i < infos.size(); i++)
                            databaseHandler.createInfo(infos.get(i));
                        instanceView();
                    } else
                        showError(pesan);
                }
            });
        }
        else
            instanceView();
    }
    private void showError(String messsage) {
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setMessage(messsage);
        alert.setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getInfo();
            }
        });
        alert.show();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_info, container, false);
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getActionMasked())
                {
                    case MotionEvent.ACTION_DOWN:
                        x1 = event.getX();
                        break;
                    case MotionEvent.ACTION_UP:
                        x2 = event.getX();
                        if(x2>x1) {
                            ((MainMenu)getActivity()).loadFragment(1);
                        }
                        else{
                            ((MainMenu)getActivity()).loadFragment(3);
                        }
                        break;
                }
                return true;
            }
        });
        listView = (ListView) view.findViewById(R.id.info_list);
        databaseHandler = new DatabaseHandler(getActivity());
        getInfo();
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }



    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }
    public void instanceView()
    {
        //List<Schedules> fromDatabase = dbHandler.getAllSchedules();
        infos = databaseHandler.getAllInfo();
        //List<Schedules> fromDatabase = sc.jadwalHariIni;
        //int count = dbHandler.getScheduleCount();
        int count = infos.size();
        if(count != 0)
            populateList();
    }
    public void populateList()
    {
        if(adapter == null)
            adapter = new InfoListAdapter();
        listView.setAdapter(adapter);
    }
    private class InfoListAdapter extends ArrayAdapter<Info> {
        public InfoListAdapter(){
            //super(Schedule.this,R.layout.listview_item,Jadwals);
            super(getActivity(),R.layout.info_item,infos);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null)
                convertView = getActivity().getLayoutInflater().inflate(R.layout.info_item, parent, false);
            final Info currentInfo = infos.get(position);
            ImageView user = (ImageView) convertView.findViewById(R.id.tl_userimage);
            if(!currentInfo.getFoto().equals(""))
                user.setImageBitmap(decodeImage(currentInfo.getFoto()));
            TextView username = (TextView) convertView.findViewById(R.id.tl_username);
            if(!currentInfo.getNama_sumber().equals(""))
                username.setText(currentInfo.getNama_sumber());
            TextView aktivitas = (TextView) convertView.findViewById(R.id.tl_aktivitas);
            if(!currentInfo.getInfo().equals(""))
                aktivitas.setText(currentInfo.getInfo());
            return convertView;
        }
    }
    private Bitmap decodeImage(String enImage)
    {
        byte[] decodedString = Base64.decode(enImage, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }
}
