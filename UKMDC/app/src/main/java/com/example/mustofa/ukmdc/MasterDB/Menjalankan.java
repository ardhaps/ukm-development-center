package com.example.mustofa.ukmdc.MasterDB;

/**
 * Created by Ardha on 4/10/2016.
 */
public class Menjalankan
{
    private int ID_user, ID_ukm;

    public Menjalankan (int ID_user, int ID_ukm)
    {
        setID_user(ID_user);
        setID_ukm(ID_ukm);
    }

    public int getID_user() {
        return ID_user;
    }

    public void setID_user(int ID_user) {
        this.ID_user = ID_user;
    }

    public int getID_ukm() {
        return ID_ukm;
    }

    public void setID_ukm(int ID_ukm) {
        this.ID_ukm = ID_ukm;
    }
}
