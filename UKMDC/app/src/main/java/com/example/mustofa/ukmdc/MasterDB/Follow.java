package com.example.mustofa.ukmdc.MasterDB;

/**
 * Created by Ardha on 4/14/2016.
 */
public class Follow
{
    private int ID_follow, ID_user, ID_user_followed;

    public Follow (int ID_follow, int ID_user, int ID_user_followed)
    {
        setID_follow (ID_follow);
        setID_user (ID_user);
        setID_user_followed (ID_user_followed);
    }

    public int getID_follow () { return ID_follow; }

    public void setID_follow(int ID_follow) {
        this.ID_follow = ID_follow;
    }

    public int getID_user () { return ID_user; }

    public void setID_user(int ID_user) {
        this.ID_user = ID_user;
    }

    public int getID_user_followed () { return ID_user_followed; }

    public void setID_user_followed(int ID_user_followed) {
        this.ID_user_followed = ID_user_followed;
    }
}
