package com.example.mustofa.ukmdc.MasterDB;

/**
 * Created by Ardha on 4/10/2016.
 */
public class Ukm
{
    private int ID_ukm, ID_user;
    private String nama_ukm, bidang_ukm, no_telp, alamat_provinsi, alamat_kota;

    public Ukm (int ID_ukm, int ID_user, String nama_ukm, String bidang_ukm, String no_telp, String alamat_provinsi, String alamat_kota)
    {
        setID_ukm(ID_ukm);
        setID_user(ID_user);
        setNama_ukm(nama_ukm);
        setBidang_ukm(bidang_ukm);
        setNo_telp(no_telp);
        setAlamat_provinsi(alamat_provinsi);
        setAlamat_kota(alamat_kota);
    }

    public int getID_ukm() {
        return ID_ukm;
    }

    public void setID_ukm(int ID_ukm) {
        this.ID_ukm = ID_ukm;
    }

    public int getID_user() {
        return ID_user;
    }

    public void setID_user(int ID_user) {
        this.ID_user = ID_user;
    }

    public String getNama_ukm() {
        return nama_ukm;
    }

    public void setNama_ukm(String nama_ukm) {
        this.nama_ukm = nama_ukm;
    }

    public String getBidang_ukm() {
        return bidang_ukm;
    }

    public void setBidang_ukm(String bidang_ukm) {
        this.bidang_ukm = bidang_ukm;
    }

    public String getNo_telp() {
        return no_telp;
    }

    public void setNo_telp(String no_telp) {
        this.no_telp = no_telp;
    }

    public String getAlamat_provinsi() {
        return alamat_provinsi;
    }

    public void setAlamat_provinsi(String alamat_provinsi) {
        this.alamat_provinsi = alamat_provinsi;
    }

    public String getAlamat_kota() {
        return alamat_kota;
    }

    public void setAlamat_kota(String alamat_kota) {
        this.alamat_kota = alamat_kota;
    }
}
