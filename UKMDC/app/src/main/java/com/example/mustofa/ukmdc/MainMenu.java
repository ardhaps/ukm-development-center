package com.example.mustofa.ukmdc;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.mustofa.ukmdc.MasterDB.DatabaseHandler;
import com.example.mustofa.ukmdc.MasterDB.GetTimelineCallback;
import com.example.mustofa.ukmdc.MasterDB.Posting;
import com.example.mustofa.ukmdc.MasterDB.ServerRequest;

import java.util.List;

public class MainMenu extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    View home,info,kolaborasi;
    TextView tv_home,tv_info,tv_kolaborasi;
    FragmentManager fragmentManager;
    ServerRequest serverRequest;
    DatabaseHandler databaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        fragmentManager = getSupportFragmentManager();
        home = (View) findViewById(R.id.ul_home);
        info = (View) findViewById(R.id.ul_info);
        kolaborasi = (View) findViewById(R.id.ul_kolaborasi);
        tv_home = (TextView) findViewById(R.id.tv_home);
        tv_info = (TextView) findViewById(R.id.tv_info);
        tv_kolaborasi = (TextView) findViewById(R.id.tv_kolaborasi);
        databaseHandler = new DatabaseHandler(this);
        serverRequest = new ServerRequest(this);
        getTimeline();
    }
    private void getTimeline(){
        if(databaseHandler.getPostingCount() == 0) {
            serverRequest.getTimeline(databaseHandler.getUser(), new GetTimelineCallback() {
                @Override
                public void Done(List<Posting> posting, String pesan) {
                    if (pesan.equals("success")) {
                        for (int i = 0; i < posting.size(); i++)
                            databaseHandler.createPosting(posting.get(i));
                        loadFragment(1);
                    } else
                        showError(pesan);
                }
            });
        }
        else
            loadFragment(1);
    }
    private void showError(String messsage) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage(messsage);
        alert.setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getTimeline();
            }
        });
        alert.show();
    }
    public void selectFragment(View v)
    {
        switch (v.getId())
        {
            case R.id.view_home:
                loadFragment(1);
                break;
            case R.id.view_info:
                loadFragment(2);
                break;
            default:
                loadFragment(3);
                break;
        }
    }
    public void loadFragment(int pos)
    {
        switch (pos){
            case 1:
                fragmentManager.beginTransaction().replace(R.id.frame_content,new TimeLine()).addToBackStack(null).commit();
                changeColor(1);
                break;
            case 2:
                fragmentManager.beginTransaction().replace(R.id.frame_content,new InfoFragment()).addToBackStack(null).commit();
                changeColor(2);
                break;
            default:
                fragmentManager.beginTransaction().replace(R.id.frame_content,new Kolaborasi()).addToBackStack(null).commit();
                changeColor(3);
                break;
        }
    }
    private void changeColor(int pos)
    {
        for(int i = 1; i < 4; i++){
            if(i == pos)
            {
                setColor(i,"aktif");
            }
            else
                setColor(i,"nonaktif");
        }
    }
    private void setColor(int pos,String status)
    {
        int warna1,warna2;
        if(status.equals("aktif")) {
            warna1 = getResources().getColor(R.color.green);
            warna2 = getResources().getColor(R.color.green);
        }
        else
        {
            warna1 = Color.BLACK;
            warna2 = Color.WHITE;
        }
        switch (pos){
            case 1:
                home.setBackgroundColor(warna2);
                tv_home.setTextColor(warna1);
                break;
            case 2:
                info.setBackgroundColor(warna2);
                tv_info.setTextColor(warna1);
                break;
            default:
                kolaborasi.setBackgroundColor(warna2);
                tv_kolaborasi.setTextColor(warna1);
                break;
        }
    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profilUkm) {
            startActivity(new Intent(this,ProfilUKM.class));
        }
        else if (id == R.id.nav_akun) {
            startActivity(new Intent(this, ProfilUser.class));
        }
        else if (id == R.id.nav_logout) {
            databaseHandler.deleteAll();
            startActivity(new Intent(this, SplashScreen.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
