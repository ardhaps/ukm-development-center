package com.example.mustofa.ukmdc;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class ProfilUser extends AppCompatActivity {

    private EditText editText1, editText2, editText3;
    private ImageView tombol, save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil_user);

        editText1 = (EditText) findViewById(R.id.editText);
        editText2 = (EditText) findViewById(R.id.editText2);
        editText3 = (EditText) findViewById(R.id.editText3);
        tombol = (ImageView) findViewById(R.id.imageView3);
        save = (ImageView) findViewById(R.id.imageView4);

        editText1.setEnabled(false);
        editText2.setEnabled(false);
        editText3.setEnabled(false);

        save.setEnabled(false);

        tombol.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                editText1.setEnabled(true);
                editText2.setEnabled(true);
                editText3.setEnabled(true);

                save.setEnabled(true);
                tombol.setEnabled(false);
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                editText1.setEnabled(false);
                editText2.setEnabled(false);
                editText3.setEnabled(false);

                tombol.setEnabled(false);
            }
        });

    }
}
