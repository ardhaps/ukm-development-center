package com.example.mustofa.ukmdc.MasterDB;

import java.util.List;

/**
 * Created by mustofa on 4/12/2016.
 */
public interface GetInfoCallback {
    void Done(List<Info> infos, String pesan);
}
