package com.example.mustofa.ukmdc.MasterDB;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mustofa on 3/3/2016.
 */
public class ServerRequest {
    ProgressDialog progressDialog;
    DatabaseHandler databaseHandler;
    public static final int CONNECTION_TIMEOUT = 1000*30;
    public static final String SERVER_ADDRESS =  "http://ukmdc.thaybah.id/public/api/v1/";
    Context context;
    public ServerRequest(Context context)
    {
        databaseHandler = new DatabaseHandler(context);
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Processing");
        progressDialog.setMessage("Please Wait");
        this.context = context;
    }
    /////////////////////////////////////////////////////////////////////////////
    //login
    public void Login(String email, String password, final GetUserCallback getUserCallback){
        //Showing the progress dialog
        progressDialog.setMessage("Processing, please wait");
        progressDialog.show();
        String message = "";
        try{
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("email",email);
            jsonObject.put("password",password);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, SERVER_ADDRESS + "user/login", jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    progressDialog.dismiss();
                    try {
                        if(response.length()!=0)
                        {
                            User user = new User(response.getInt("ID_user"),response.getString("nama_lengkap"), response.getString("email"),
                                    response.getString("password"), response.getString("pekerjaan"), response.getString("level_user"),
                                    response.getString("modal"), response.getString("tgl_lahir"), response.getString("foto_user"),
                                    response.getInt("isProduct"),response.getInt("isIdea"));
                            getUserCallback.Done(user,response.getString("status"));
                        }
                    } catch (JSONException e) {
                        getUserCallback.Done(null, e.getMessage());
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    getUserCallback.Done(null, error.toString());
                    //Toast.makeText(context, error.getMessage().toString(), Toast.LENGTH_LONG).show();
                }
            });

            //Creating a Request Queue
            RequestQueue requestQueue = Volley.newRequestQueue(context);

            //Adding request to the queue
            requestQueue.add(jsonObjectRequest);
        }
        catch (JSONException e) {
            e.printStackTrace();
            message = e.getMessage();
        }
    }
    ///////////////////////////////////////////////////////////////////////////////////
    //register
    public void Register(User user, Ukm ukm, final GetUserCallback getUserCallback){
        //Showing the progress dialog
        progressDialog.setMessage("Processing, please wait");
        progressDialog.show();
        String message = "";
        try{
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("email",user.getEmail());
            jsonObject.put("password",user.getPassword());
            jsonObject.put("nama_lengkap",user.getNama_lengkap());
            jsonObject.put("pekerjaan",user.getPekerjaan());
            jsonObject.put("tgl_lahir",user.getTgl_lahir());
            jsonObject.put("level_user",user.getLevel_user());
            jsonObject.put("isProduct",user.getIsProduct());
            jsonObject.put("isIdea",user.getIsIdea());
            jsonObject.put("modal",user.getModal());
            jsonObject.put("nama_ukm",ukm.getNama_ukm());
            jsonObject.put("bidang_ukm",ukm.getBidang_ukm());
            jsonObject.put("no_telp",ukm.getNo_telp());
            jsonObject.put("alamat_provinsi",ukm.getAlamat_provinsi());
            jsonObject.put("alamat_kota",ukm.getAlamat_kota());
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, SERVER_ADDRESS + "user/register", jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    progressDialog.dismiss();
                    try {
                        if(response.length()!=0)
                        {
                            getUserCallback.Done(null,response.getString("status"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    getUserCallback.Done(null, error.toString());
                    //Toast.makeText(context, error.getMessage().toString(), Toast.LENGTH_LONG).show();
                }
            });

            //Creating a Request Queue
            RequestQueue requestQueue = Volley.newRequestQueue(context);

            //Adding request to the queue
            requestQueue.add(jsonObjectRequest);
        }
        catch (JSONException e) {
            e.printStackTrace();
            message = e.getMessage();
        }
    }
    ///////////////////////////////////////////////////////////////////////////////////
    //Timeline
    public void getTimeline(User user, final GetTimelineCallback getTimelineCallback){
        //Showing the progress dialog
        progressDialog.setMessage("Processing, please wait");
        progressDialog.show();
        String message = "";
        final List<Posting> postings = new ArrayList<Posting>();
        try{
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("ID_user", user.getID_user());
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST, SERVER_ADDRESS + "user/timeline", jsonObject, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    progressDialog.dismiss();
                    try {
                        for (int i = 0; i < response.length(); i++) {
                            JSONObject jsonResponse = response.getJSONObject(i);
                            if(jsonResponse.length()!=0)
                            {
                                postings.add(new Posting(jsonResponse.getInt("ID_posting"),
                                        jsonResponse.getInt("ID_user"),jsonResponse.getString("nama_lengkap"),
                                        jsonResponse.getString("isi"),
                                        jsonResponse.getString("jam_posting"),
                                        jsonResponse.getString("foto_posting"),
                                        0));
                            }
                        }
                        getTimelineCallback.Done(postings,"success");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    getTimelineCallback.Done(null, error.toString());
                    //Toast.makeText(context, error.getMessage().toString(), Toast.LENGTH_LONG).show();
                }
            });

            //Creating a Request Queue
            RequestQueue requestQueue = Volley.newRequestQueue(context);

            //Adding request to the queue
            requestQueue.add(jsonArrayRequest);
        }
        catch (JSONException e) {
            e.printStackTrace();
            message = e.getMessage();
        }
    }
    ///////////////////////////////////////////////////////////////////////////////////
    //InfoFragment
    public void getInfo(final GetInfoCallback getInfoCallback){
        //Showing the progress dialog
        progressDialog.setMessage("Processing, please wait");
        progressDialog.show();
        String message = "";
        final List<Info> infos = new ArrayList<Info>();
        try{
            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, SERVER_ADDRESS + "user/daftar_info", new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    progressDialog.dismiss();
                    try {
                        for (int i = 0; i < response.length(); i++) {
                            JSONObject jsonResponse = response.getJSONObject(i);
                            if(jsonResponse.length()!=0)
                            {
                                infos.add(new Info(jsonResponse.getInt("ID_info"),
                                        jsonResponse.getString("nama_sumber"),
                                        jsonResponse.getString("info"),
                                        jsonResponse.getString("time"),
                                        jsonResponse.getString("date"),
                                        jsonResponse.getString("foto_sumber")));
                            }
                        }
                        getInfoCallback.Done(infos,"success");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                    getInfoCallback.Done(null, error.toString());
                    //Toast.makeText(context, error.getMessage().toString(), Toast.LENGTH_LONG).show();
                }
            });

            //Creating a Request Queue
            RequestQueue requestQueue = Volley.newRequestQueue(context);

            //Adding request to the queue
            requestQueue.add(jsonArrayRequest);
        }
        catch (Exception e) {
            e.printStackTrace();
            message = e.getMessage();
        }
    }
}