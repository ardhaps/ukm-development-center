package com.example.mustofa.ukmdc.MasterDB;

/**
 * Created by mustofa on 4/10/2016.
 */
public class User
{
    private int ID_user, isProduct, isIdea;
    private String nama_lengkap, email, password, pekerjaan, level_user, modal, tgl_lahir, foto_user;

    public User (int ID, String nama_lengkap, String email, String password, String pekerjaan, String level_user, String modal, String tgl_lahir, String foto_user, int isProduct, int isIdea)
    {
        setID_user(ID);
        setNama_lengkap(nama_lengkap);
        setEmail(email);
        setPassword(password);
        setPekerjaan(pekerjaan);
        setLevel_user(level_user);
        setModal(modal);
        setTgl_lahir(tgl_lahir);
        setFoto_user(foto_user);
        setIsProduct(isProduct);
        setIsIdea(isIdea);
    }

    public int getID_user()
    {
        return ID_user;
    }

    public void setID_user(int ID_user)
    {
        this.ID_user = ID_user;
    }

    public String getNama_lengkap()
    {
        return nama_lengkap;
    }

    public void setNama_lengkap (String nama_lengkap)
    {
        this.nama_lengkap = nama_lengkap;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getPassword ()
    {
        return password;
    }

    public void setPassword (String password)
    {
        this.password = password;
    }

    public String getPekerjaan ()
    {
        return pekerjaan;
    }

    public void setPekerjaan (String pekerjaan)
    {
        this.pekerjaan = pekerjaan;
    }

    public String getLevel_user()
    {
        return level_user;
    }

    public void setLevel_user(String level_user)
    {
        this.level_user = level_user;
    }

    public String getModal ()
    {
        return modal;
    }

    public void setModal (String modal)
    {
        this.modal = modal;
    }

    public String getTgl_lahir()
    {
        return tgl_lahir;
    }

    public void setTgl_lahir (String tgl_lahir)
    {
        this.tgl_lahir = tgl_lahir;
    }

    public String getFoto_user ()
    {
        return foto_user;
    }

    public void setFoto_user (String foto_user)
    {
        this.foto_user = foto_user;
    }

    public int getIsProduct ()
    {
        return isProduct;
    }

    public void setIsProduct (int isProduct)
    {
        this.isProduct = isProduct;
    }

    public int getIsIdea ()
    {
        return isIdea;
    }

    public void setIsIdea (int isIdea)
    {
        this.isIdea = isIdea;
    }
}
