-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Inang: 127.0.0.1
-- Waktu pembuatan: 01 Apr 2016 pada 15.01
-- Versi Server: 5.6.14
-- Versi PHP: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Basis data: `ukmdc`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `menjalankan`
--

CREATE TABLE IF NOT EXISTS `menjalankan` (
  `ID_user` int(10) NOT NULL,
  `ID_ukm` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `posting`
--

CREATE TABLE IF NOT EXISTS `posting` (
  `ID_posting` int(10) NOT NULL AUTO_INCREMENT,
  `ID_user` int(10) NOT NULL,
  `isi` varchar(150) NOT NULL,
  `tanggal_posting` date NOT NULL,
  `isBranding` tinyint(1) NOT NULL,
  `foto` blob NOT NULL,
  PRIMARY KEY (`ID_posting`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rekomendasi`
--

CREATE TABLE IF NOT EXISTS `rekomendasi` (
  `ID_rekomendasi` int(10) NOT NULL AUTO_INCREMENT,
  `ID_user` int(10) NOT NULL,
  PRIMARY KEY (`ID_rekomendasi`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ukm`
--

CREATE TABLE IF NOT EXISTS `ukm` (
  `ID_ukm` int(10) NOT NULL AUTO_INCREMENT,
  `nama_ukm` varchar(40) NOT NULL,
  `bidang_ukm` varchar(30) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `alamat_provinsi` varchar(30) NOT NULL,
  `alamat_kota` varchar(35) NOT NULL,
  PRIMARY KEY (`ID_ukm`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `ID_user` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL,
  `email` varchar(30) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `pekerjaan` varchar(40) NOT NULL,
  `tanggal_lahir` int(11) NOT NULL,
  `bulan_lahir` int(11) NOT NULL,
  `tahun_lahir` int(11) NOT NULL,
  `level_user` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `isProduct` tinyint(1) NOT NULL,
  `idIdea` tinyint(1) NOT NULL,
  `modal` varchar(20) NOT NULL,
  `bidang` varchar(30) NOT NULL,
  PRIMARY KEY (`ID_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`ID_user`, `username`, `email`, `nama_lengkap`, `pekerjaan`, `tanggal_lahir`, `bulan_lahir`, `tahun_lahir`, `level_user`, `password`, `isProduct`, `idIdea`, `modal`, `bidang`) VALUES
(0000000001, 'Alif', 'alif.sip@gmail.com', 'Alif Ridho Musthafa', 'Mahasiswa', 8, 11, 1993, 'Perintis Bisnis', 'megaphone', 0, 0, '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
